package com.pt4d;

import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricDetail;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.activiti.engine.impl.persistence.entity.HistoricFormPropertyEntity;
import org.activiti.engine.impl.persistence.entity.VariableInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.spring.impl.test.SpringActivitiTestCase;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Pt4dActivitiApplicationTests extends SpringActivitiTestCase {
	
	@Autowired
	@Rule
	public ActivitiRule activitiSpringRule;
	
    @Autowired
    protected ProcessEngine processEngine;
	
    @Autowired
    protected RuntimeService runtimeService;
	
    @Autowired
    protected RepositoryService repositoryService;
    
    @Autowired
    protected TaskService taskService;
    
    @Autowired
    protected HistoryService historyService;
    
    @Autowired
    protected ManagementService managementService;
    
    @Autowired
    protected IdentityService identityService;
    
    
    @Autowired
    protected FormService formService;
	
	@Test
	public void contextLoads() {
		Assert.assertNotNull(processEngine);
		Assert.assertNotNull(repositoryService);
		Assert.assertNotNull(taskService);
		Assert.assertNotNull(historyService);
		Assert.assertNotNull(managementService);
		Assert.assertNotNull(identityService);
	}
	
	@Test
	public void testFindTask() throws Exception {
		TaskService taskService = processEngine.getTaskService();
		List<Task> taskList = taskService
				.createTaskQuery().list();
		Assert.assertTrue(taskList.size() > 0);
	}
	
	//COMPLETE TASK
	public void completeTaskByProcessInstance(String processInstanceId, Map<String, Object> taskVars) {
		System.out.println("START - COMPLETE TASK BY PROCESS ID " + processInstanceId);
		List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
		Assert.assertEquals(1, tasks.size());
		taskService.complete(tasks.get(0).getId(), taskVars);
		System.out.println("END - COMPLETE TASK BY PROCESS ID " + processInstanceId);
	}
	
	public void submitTaskFormByProcessInstance(String processInstanceId, Map<String, String> formProperties) {
		System.out.println("START - SUBMIT TASK FORM BY PROCESS ID " + processInstanceId);
		List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
		Assert.assertEquals(1, tasks.size());
		formService.submitTaskFormData(tasks.get(0).getId(), formProperties);
		System.out.println("END - SUBMIT TASK FORM BY PROCESS ID " + processInstanceId);
	}
	
	//HISOTRIC TASKS
	public List<HistoricTaskInstance> displayHistoricTaskByProcessId(String processInstanceId) {
		List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().processInstanceId(processInstanceId).list();
		System.out.println("START - DISPLAY ALL HISTORIC TASK BY PROCESS ID " + processInstanceId);
		for(HistoricTaskInstance elem : list) {
			System.out.println(elem);
		}
		System.out.println("END - DISPLAY ALL HISTORIC TASK BY PROCESS ID " + processInstanceId);
		return list;
	}	

	public List<HistoricTaskInstance> displayHistoricTaskByAssignee(String assigneeId) {
		List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery().taskAssignee(assigneeId).list();
		System.out.println("START - DISPLAY ALL HISTORIC TASK BY ASSIGNEE ID " + assigneeId);
		for(HistoricTaskInstance elem : list) {
			System.out.println(elem);
		}
		System.out.println("END - DISPLAY ALL HISTORIC TASK BY ASSIGNEE ID " + assigneeId);
		return list;
	}
	
	//HISTORIC ACTIVITIES
	public List<HistoricActivityInstance> displayHistoricActivityByProcessId(String processInstanceId) {
		List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery().processInstanceId(processInstanceId).list();
		System.out.println("START - DISPLAY ALL HISTORIC ACTIVITY BY PROCESS ID " + processInstanceId);
		for(HistoricActivityInstance elem : list) {
			System.out.println(elem);
		}
		System.out.println("END - DISPLAY ALL HISTORIC ACTIVITY BY PROCESS ID " + processInstanceId);
		return list;
	}
	
	//HISTORIC VARIABLES
	public List<HistoricVariableInstance> displayAllHistoricVariable() {
		List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery().list();
		System.out.println("START - DISPLAY ALL HISTORIC VARIABLE");		
		for(HistoricVariableInstance elem : list) {
			System.out.println(elem);
		}
		
		System.out.println("END - DISPLAY ALL HISTORIC VARIABLE");
		return list;
	}
	
	public List<HistoricVariableInstance> displayHistoricVariableByExecutionId(String executionId) {
		List<HistoricVariableInstance> list = historyService.createHistoricVariableInstanceQuery().executionId(executionId).list();
		System.out.println("START - DISPLAY HISTORIC VARIABLE BY EXECUTION ID " + executionId);
		for(HistoricVariableInstance elem : list) {
			System.out.println(elem);
		}
		System.out.println("END - DISPLAY HISTORIC VARIABLE BY EXECUTION ID " + executionId);
		return list;
	}
	
	public List<HistoricDetail> displayHistoricVariableByTaskId(String taskId) {
		List<HistoricDetail> list = historyService.createHistoricDetailQuery().variableUpdates().taskId(taskId).list();
		System.out.println("START - DISPLAY HISTORIC VARIABLE BY TASK ID " + taskId);
		for(HistoricDetail elem : list) {
			System.out.println(elem);
		}
		System.out.println("END - DISPLAY HISTORIC VARIABLE BY TASK ID " + taskId);
		return list;
	}
	
	public List<HistoricDetail> displayFormPropertiesByTaskId(String taskId) {
		List<HistoricDetail> list = historyService.createHistoricDetailQuery().formProperties().taskId(taskId).list();
		System.out.println("START - DISPLAY HISTORIC FORM PROPERTIES BY TASK ID " + taskId);
		for(HistoricDetail elem : list) {
			HistoricFormPropertyEntity formEntity = (HistoricFormPropertyEntity) elem;
			System.out.println(formEntity.getPropertyValue());
		}
		System.out.println("END - DISPLAY HISTORIC FORM PROPERTIES BY TASK ID " + taskId);
		return list;
	}
	
	public List<HistoricDetail> displayHistoricVariableByActivityId(String activityId) {
		List<HistoricDetail> list = historyService.createHistoricDetailQuery().variableUpdates().activityInstanceId(activityId).list();
		System.out.println("START - DISPLAY HISTORIC VARIABLE BY ACTIVITY ID " + activityId);
		for(HistoricDetail elem : list) {
			System.out.println(elem);
		}
		System.out.println("END - DISPLAY HISTORIC VARIABLE BY ACTIVITY ID " + activityId);
		return list;
	}
	
	public List<HistoricDetail> displayFormPropertiesByActivityId(String activityId) {
		List<HistoricDetail> list = historyService.createHistoricDetailQuery().formProperties().activityInstanceId(activityId).list();
		System.out.println("START - DISPLAY HISTORIC FORM PROPERTIES BY ACTIVITY ID " + activityId);
		for(HistoricDetail elem : list) {
			HistoricFormPropertyEntity formEntity = (HistoricFormPropertyEntity) elem;
			System.out.println(formEntity.getPropertyValue());
		}
		System.out.println("END - DISPLAY HISTORIC FORM PROPERTIES BY ACTIVITY ID " + activityId);
		return list;
	}
	
	//VARIABLES
	public Map<String, VariableInstance> displayVariablesByExecutionId(String executionId) {
		Map<String, VariableInstance> map = runtimeService.getVariableInstances(executionId);
		System.out.println("START - DISPLAY VARIABLE BY EXECUTION ID " + executionId);
		for(Map.Entry<String, VariableInstance>entry : map.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
		}		
		System.out.println("END - DISPLAY VARIABLE BY EXECUTION ID " + executionId);
		return map;
	}
	
	//PROCESS DEFINITION
	public String getProcessDefinitionIdFromKey(String processDefinitionId) {
		return repositoryService.createProcessDefinitionQuery().processDefinitionKey("formProcess").latestVersion().singleResult().getId();
	}
}
