package com.pt4d;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.activiti.types.SerializeAsJson;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FormTest extends Pt4dActivitiApplicationTests {
	
	@Autowired
	@Rule
	public ActivitiRule activitiSpringRule;
	
	@Test
	@Deployment
	public void processTest() {
		String processDefinitionId = getProcessDefinitionIdFromKey("formProcess");
		
		System.out.println(identityService.createGroupQuery().groupId("04k668n33hgmt6x").singleResult());

		Map<String, Object> processVars = new HashMap<String, Object>();
		Map<String, String> formProperties = new HashMap<String, String>();
		
		StartFormData startFormData = formService.getStartFormData(processDefinitionId);
		
		for(FormProperty formProperty : startFormData.getFormProperties()) {
			System.out.println(formProperty.getName());
		}
		
		Pt4dFileManager pt4dFileManager = new Pt4dFileManager();
		Pt4dFile file;
		try {
			//file = pt4dFileManager.getFileById("190Gcg0NqGadBgCzY9M3xWqof3KHgtyiT9gcuToddhdw");
			processVars.put("pt4d_file_id", "190Gcg0NqGadBgCzY9M3xWqof3KHgtyiT9gcuToddhdw");
			ProcessInstance ins = runtimeService.startProcessInstanceById(processDefinitionId, processVars);
			displayVariablesByExecutionId(ins.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		formProperties.put("service", "hr");
		formProperties.put("name", "DA Name");
		formProperties.put("new prop", "My new prop");
		ProcessInstance processInstance = formService.submitStartFormData(processDefinitionId, formProperties);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = mapper.createObjectNode().put("hihihi", "mon json");;
		runtimeService.setVariable(processInstance.getId(), "my_json", json);
		
		displayVariablesByExecutionId(processInstance.getId());
		
		formProperties = new HashMap<String, String>();
		formProperties.put("question", "this is my question ?");
		submitTaskFormByProcessInstance(processInstance.getId(), formProperties);
		
		Object v = runtimeService.getVariable(processInstance.getId(), "formProperties");
		System.out.println(v);
		
		displayVariablesByExecutionId(processInstance.getId());
		displayHistoricActivityByProcessId(processInstance.getProcessInstanceId());
		
		formProperties = new HashMap<String, String>();
		formProperties.put("question", "this is my question 222 ?");
		submitTaskFormByProcessInstance(processInstance.getId(), formProperties);
		
		displayHistoricVariableByExecutionId(processInstance.getId());
		
		List<HistoricActivityInstance> activities = displayHistoricActivityByProcessId(processInstance.getId());
		
		for (HistoricActivityInstance activity : activities) {
			displayHistoricVariableByActivityId(activity.getId());
			displayFormPropertiesByActivityId(activity.getId());
		}
		
		System.out.println(processInstance.getName());
		System.out.println(historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstance.getId()).list().get(0).getName());
	}
}
