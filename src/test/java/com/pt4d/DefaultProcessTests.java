package com.pt4d;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DefaultProcessTests extends Pt4dActivitiApplicationTests {
	
	@Autowired
	@Rule
	public ActivitiRule activitiSpringRule;
	
	@Test
	@Deployment
	public void generatedProcessTest() {
		Assert.assertNotNull(processEngine);

		Map<String, Object> processVars = new HashMap<String, Object>();
		
		identityService.setAuthenticatedUserId("admin");
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("wkf-2", processVars);
		
		System.out.println(runtimeService.getVariables(processInstance.getId()));
		
		//Task 1
		Map<String, Object> taskVars = new HashMap<String, Object>();
		taskVars.put("choice1", "ok");
		
		List<Task> taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(1, taskList.size());
		
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		//Task 2
		taskVars = new HashMap<String, Object>();
		taskVars.put("choice2", "ok");
		
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(1, taskList.size());
		
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		//Task 3
		taskVars = new HashMap<String, Object>();
		taskVars.put("choice3", "ok");
		
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(1, taskList.size());;
		
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		//Final check
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(0, taskList.size());
	}
	
	@Test
	@Deployment
	public void pt4dDefaultProcessTest() {
		Assert.assertNotNull(processEngine);
		
		Map<String, Object> processVars = new HashMap<String, Object>();
		
		processVars.put("nb_step", 3);
		
		processVars.put("step_0_name", "My Step one !!!");
		processVars.put("step_0_assignee_id", "admin");
		processVars.put("step_0_candidate_users", "");
		processVars.put("step_0_candidate_groups", "test");
		
		processVars.put("step_1_name", "My Step two");
		processVars.put("step_1_assignee_id", "");
		processVars.put("step_1_candidate_users", "user2");
		processVars.put("step_1_candidate_groups", "");

		processVars.put("step_2_name", "My Step three");
		processVars.put("step_2_assignee_id", "");
		processVars.put("step_2_candidate_users", "user2");
		processVars.put("step_2_candidate_groups", "");
		
		identityService.setAuthenticatedUserId("admin");
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("pt4dDefaultProcess", processVars);
		
		User user = identityService.createUserQuery().userId("admin").singleResult();
		System.out.println(user.getEmail());
		
		System.out.println(runtimeService.getVariables(processInstance.getId()));
		
		Map<String, Object> taskVars = new HashMap<String, Object>();
		taskVars.put("result", "accept");
		
		List<Task> taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(taskList.size(), 1);
		
		System.out.println("Task 1");
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(taskList.size(), 1);
		
		List<Group> userGroupList = identityService.createGroupQuery().groupMember("user1").list();		
		List<String> userGroupIdList = userGroupList.stream().map(Group::getId).collect(Collectors.toList());
		List<Task> userByGroupTaskList = taskService.createTaskQuery().taskCandidateGroupIn(userGroupIdList).list();
		
		List<Task> userTaskList = taskService.createTaskQuery().taskInvolvedUser("user1").list();
		
		userTaskList.addAll(userByGroupTaskList);
		
		Assert.assertEquals(userTaskList.size(), 1);
		
		System.out.println(runtimeService.getVariables(processInstance.getId()).get("status_step"));
		
		System.out.println("Task 2");
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(taskList.size(), 1);
		
		System.out.println(runtimeService.getVariables(processInstance.getId()).get("status_step"));
		
		System.out.println("Task 3");
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		System.out.println(taskList);
		
		Assert.assertEquals(taskList.size(), 0);
		
		System.out.println(runtimeService.getVariables(processInstance.getId()).get("status_step"));
	}

}
