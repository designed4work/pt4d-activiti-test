package com.pt4d;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;


@RunWith(SpringRunner.class)
@SpringBootTest
public class Pt4dApiTest extends Pt4dActivitiApplicationTests {
	
	@Autowired
	@Rule
	public ActivitiRule activitiSpringRule;
	
	@Autowired
	public Pt4dFileManager pt4dFileManager;
	
	@Test
	public void apiTest() {
		try {
			Pt4dFile file = pt4dFileManager.getFileById("1wPc2l944z35_a4eTIJU06jV1xvnd3yIDm9keUGIMf1I");
			
			System.out.println(file.getAttributes());
			System.out.println(file.getIsActive());
			System.out.println(file.getTags());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
