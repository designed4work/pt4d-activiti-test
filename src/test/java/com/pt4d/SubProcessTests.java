package com.pt4d;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SubProcessTests extends Pt4dActivitiApplicationTests {
	
	@Autowired
	@Rule
	public ActivitiRule activitiSpringRule;
	
	@Test
	@Deployment
	public void mainTest() {
		Assert.assertNotNull(processEngine);

		Map<String, Object> processVars = new HashMap<String, Object>();
		
		identityService.setAuthenticatedUserId("admin");
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("encapsulation-process");
		
		System.out.println(runtimeService.getVariables(processInstance.getId()));
		
		displayHistoricTaskByProcessId(processInstance.getId());
		
		//Task 1
		Map<String, Object> taskVars = new HashMap<String, Object>();
		
		List<Task> taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(1, taskList.size());
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		runtimeService.setVariable(processInstance.getId(), "onfly", "hihihi");
		
		//Task 2
		taskVars = new HashMap<String, Object>();
		
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(1, taskList.size());
		taskService.complete(taskList.get(0).getId(), taskVars);
		
		displayHistoricTaskByProcessId(processInstance.getId());
		displayHistoricActivityByProcessId(processInstance.getId());
		displayHistoricVariableByExecutionId(processInstance.getId());
		
		//Final check
		taskList = taskService.createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();
		Assert.assertEquals(0, taskList.size());
	}

}
