package com.pt4d;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricVariableInstance;
import org.activiti.engine.history.HistoricVariableInstanceQuery;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.test.JobTestHelper;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.pt4d.activiti.beans.Pt4dFileManager;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ErrorProcessTests extends Pt4dActivitiApplicationTests {
	
	@Autowired
	@Rule
	public ActivitiRule activitiSpringRule;
	
	@Test
	@Deployment
	public void errorTest() {
		displayAllHistoricVariable();

		Map<String, Object> processVars = new HashMap<String, Object>();
		
		identityService.setAuthenticatedUserId("admi");
		
		User user = identityService.createUserQuery().userId("102474636055814908148").singleResult();
		System.out.println(user.getEmail());
		
		processVars.put("file_id", "1wPc2l944z35_a4eTIJU06jV1xvnd3yIDm9keUGIMf1I"); //active
		//processVars.put("file_id", "0B2duDe2NHBlkM0RRUC02eFh5Z1U"); //null
		
		ProcessInstance processInstance=null;
		processInstance = runtimeService.startProcessInstanceByKey("errorProcess", processVars);
		
		//displayHistoricTaskByProcessId(processInstance.getId());
		//displayHistoricVariableByExecutionId(processInstance.getId());
		
		completeTaskByProcessInstance(processInstance.getId(), null);
		
		displayHistoricActivityByProcessId(processInstance.getId());
		displayHistoricTaskByProcessId(processInstance.getId());
		displayHistoricVariableByExecutionId(processInstance.getId());
		
	}
}
