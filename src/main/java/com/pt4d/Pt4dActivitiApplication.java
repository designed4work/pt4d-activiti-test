package com.pt4d;

import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Pt4dActivitiApplication {

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(Pt4dActivitiApplication.class, args);
		
		ProcessEngine processEngine = applicationContext.getBean(ProcessEngine.class);
		
		System.out.println("process engine init");
		
		TaskService taskService = processEngine.getTaskService();
		List<Task> taskList = taskService
				.createTaskQuery().list();
		
		for (Task task : taskList) {
			System.out.println(task);
		}
		
		System.out.println("process engine end");
	}
	
	@Bean
    public CommandLineRunner init(final RepositoryService repositoryService,
                                  final RuntimeService runtimeService,
                                  final TaskService taskService) {
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                System.out.println("Number of process definitions : " + repositoryService.createProcessDefinitionQuery().count());
                System.out.println("Number of tasks : " + taskService.createTaskQuery().count());
           }
        };
	}
}
