package com.pt4d;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
@ImportResource("classpath:activiti-spring-config.xml")
public class Config {

    @Autowired
    private DataSource dataSource;

}
